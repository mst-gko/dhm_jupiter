import time
import os
import math

import requests
import configparser
import concurrent.futures
import psycopg2 as pg
import pandas as pd
from sqlalchemy import create_engine


class Database:

    def __init__(self):
        self.ini_dir = r'F:/GKO/data/grukos/db_credentials/writer/writer.ini'
        self.ini_section = 'JUPITER'
        self.user, self.password, self.host, self.port, self.database \
            = self.parse_db_credentials()
        self.engine = self.create_engine()

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host=self.host,
                port=self.port,
                database=self.database,
                user=self.user,
                password=self.password
            )
            print('Connected to database: ' + self.database)
            return pg_con
        except Exception as e:
            print('Unable to connect to database: ' + self.database)
            print(e)

    def run_pg_sql(self, sql):
        with self.connect_to_pg_db() as con:
            with con.cursor() as cur:
                cur.execute(sql)
                con.commit()

    def parse_db_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        password = config[f'{self.ini_section}']['password']
        user = config[f'{self.ini_section}']['userid']
        host = config[f'{self.ini_section}']['host']
        port = config[f'{self.ini_section}']['port']
        database = config[f'{self.ini_section}']['databasename']

        return user, password, host, port, database

    def parse_datafordeler_credentials(self):
        config = configparser.ConfigParser()
        config.read(self.ini_dir)

        password = config['DATAFORDELER']['password']
        user = config['DATAFORDELER']['userid']

        return user, password

    def create_engine(self):
        con = create_engine(f'postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}')
        return con

    def fetch_boreholes(self, chunk):
        sql = f'''
              SELECT DISTINCT
                  b.boreholeid, b.elevation, 
                  b.xutm32euref89, b.yutm32euref89, 
                  current_date::text as datetime,
                  ST_AsText(geom) AS geom_wkt 
              FROM jupiter.borehole b 
              LEFT JOIN dhm.dhm_jupiter_temp d ON d.boreholeid::numeric = b.boreholeid
              WHERE d.boreholeid IS NULL
              ORDER by boreholeid DESC NULLS LAST
              limit {str(chunk)}
              ;
        '''
        sql_2 = f'''
              SELECT DISTINCT
                  b.boreholeid, b.elevation, 
                  b.xutm32euref89, b.yutm32euref89,
                  current_date::text as datetime,
                  ST_AsText(geom) AS geom_wkt 
              FROM jupiter.borehole b 
              ORDER by boreholeid DESC NULLS LAST
              limit {str(chunk)}
        '''

        with self.connect_to_pg_db() as con:
            try:
                df = pd.read_sql_query(sql, con=con)
            except Exception as e:
                print(e)
                df = pd.read_sql_query(sql_2, con=con)
            return df

    def fetch_response(self, df):
        list_ele_dhm = []
        list_dist_xy = []
        list_dist_z = []
        list_ele_jup = []

        for index, row in df.iterrows():
            x_jup = row['xutm32euref89']
            y_jup = row['yutm32euref89']
            borid = int(row['boreholeid'])
            geom = row['geom_wkt']
            ele_jup = row['elevation']
            srid = 25832

            try:
                # fetch rest response from datafordeler
                datafordeler_user, datafordeler_password = self.parse_datafordeler_credentials()
                url = f'https://services.datafordeler.dk/DHMTerraen/DHMKoter/1.0.0/GEOREST/HentKoter?'
                url += f'username={datafordeler_user}&'
                url += f'password={datafordeler_password}&'
                url += f'geop={geom}&'
                url += f'georef=EPSG:{str(srid)}'
                json_response = requests.get(url).json()

                # fetch elevation from response
                ele_dhm = json_response['HentKoterRespons']['data'][0]['kote']

                # set elevation to null if -9999
                if float(ele_dhm) == -9999:
                    ele_dhm = None
                dist_z = abs(float(ele_dhm) - ele_jup)

                # fetch xy coords from response
                coords_dhm = json_response['HentKoterRespons']['data'][0]['geop']
                x_dhm = float(coords_dhm.split(',')[0])
                y_dhm = float(coords_dhm.split(',')[1])
                dist_xy = round(math.sqrt((y_dhm - y_jup) ** 2 + (x_dhm - x_jup) ** 2), 2)

            except Exception as e:
                print(e)
                ele_dhm = None
                dist_z = None
                dist_xy = None
                x_dhm = None
                y_dhm = None

            print(f'borid:{borid}, x_jup:{x_jup}, y_jup:{y_jup}, ele_jup:{ele_jup}, ele_dhm:{ele_dhm}, '
                  f'x_dhm:{x_dhm}, y_dhm:{y_dhm}, dist_xy:{dist_xy}, dist_z:{dist_z}')
            list_ele_dhm.append(ele_dhm)
            list_dist_xy.append(dist_xy)
            list_dist_z.append(dist_z)
            list_ele_jup.append(ele_jup)

        df['z_dhm'] = list_ele_dhm
        df['z_jup'] = list_ele_jup
        df['dist_z'] = list_dist_z
        df['dist_xy'] = list_dist_xy
        df.pop('xutm32euref89')
        df.pop('yutm32euref89')
        df.pop('elevation')
        df.pop('geom_wkt')
        df.to_sql(
            name='dhm_jupiter_temp',
            con=self.engine, schema='dhm',
            if_exists='append', index=False,
            chunksize=5000, method='multi'
        )

    def test_production_theme(self, prod_table='dhm.dhm_jupiter', temp_table='dhm.dhm_jupiter_temp'):
        sql_test_size = f'''
                SELECT 
                    CASE 
                        WHEN pg_total_relation_size('{prod_table}') <= pg_total_relation_size('{temp_table}') 
                            THEN 1
                        ELSE 0
                        END AS prod_test
        '''
        with self.connect_to_pg_db() as con:
            df_test = pd.read_sql_query(sql_test_size, con=con)
            test_prod_theme = df_test['prod_test'].iloc[0]
            test_prod_theme = int(test_prod_theme)

        return test_prod_theme


t1 = time.perf_counter()

num_boreholes = 'ALL'
d = Database()
d.run_pg_sql(sql='DROP TABLE IF EXISTS dhm.dhm_jupiter_temp')
df_all = d.fetch_boreholes(num_boreholes)

num_threads = 50
df_list = []
for i in range(num_threads):
    fraction = 1/(num_threads-i)
    df_frac = df_all.sample(frac=fraction)
    df_list.append(df_frac)
    df_all = df_all.drop(df_frac.index)

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(d.fetch_response, df_list)

comment_temp_schema = f"""
        DO
        $do$
            BEGIN
                EXECUTE 'COMMENT ON TABLE dhm.dhm_jupiter_temp IS '''
             || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
             || ' simak'
             || ' Table (NOT PRODUCTION) containing all elevation values from DHM model at jupiter borehole locations'
             || '''';
            END
        $do$
        ;
"""
d.run_pg_sql(sql=comment_temp_schema)

test_prod = d.test_production_theme()
if test_prod == 1:
    sql_replace_prod_table = """
        BEGIN;
            DROP TABLE IF EXISTS dhm.dhm_jupiter;
    
            CREATE TABLE dhm.dhm_jupiter AS
                (
                    SELECT *
                    FROM dhm.dhm_jupiter_temp
                )
            ;
            
            DO
            $do$
                BEGIN
                    EXECUTE 'COMMENT ON TABLE dhm.dhm_jupiter IS '''
                 || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
                 || ' simak'
                 || ' Table (PRODUCTION) containing all elevation values from DHM model at jupiter borehole locations'
                 || '''';
                END
            $do$
            ;
    
        COMMIT;    
    """
    d.run_pg_sql(sql=sql_replace_prod_table)

t2 = time.perf_counter()
elapsed = round(t2 - t1)
print(f'\n{os.path.basename(__file__)} executed in {elapsed} s')
